# CFB Sim - Player Generator

This node module generates the player data used in the CFB-SIM

## Getting Started

After cloning the project, cd into the project folder and run NPM install.
App settings are located in settings.js

### Prerequisites

Node

## Running the generator

Running player-generator.js without any flags will generate a default number of players and output them to the console.
- -g [integer] - Generate [integer] players.
- -p [string] - Generate players of the [string] position. Valid strings: QB,RB,WR,TE,K,P,OL,DL,LB,DB
- -c [string] - All players will be generated with a city named [string]. Single name cities like Monroe can be entered without quotes, multiple name cities like "Ann Arbor" need to be entered with quotes.
- -s [string] - All players will be generated hailing from state [string]. All two letter state abbreviations accepted.
- -b [integer,integer] - Generate players born between [integer] and [integer] year.
- -w - Writes the generated players to the output file. Writes to the default file if no file indicated.
- -d - Deletes the chosen file. Deletes the default file if no file indicated.
        **Cannot be run with any other commands. Delete supercedes all.**
- -f - Name of the file to manipulate.
- -h - Displays the helpfile.
- -v - Shows the version number.

## TO-DO
- Generate abilities and potential abilities
- Create functions that create different 'tiers' of players
- Create functions that create different 'tiers' of player potential
- Considering mandating that a filename be indicated when using the -d flag to prevent accidental deletions of the default file

## Completed TO-DOs
- Refactor the generateNumber lookup table to be able to generate all position specific variables.
- Break functions out into specialized files.
- Allow generation of individual positions only.
- Allow the cities flag to pass in a unique city name for all generated players.
- Players have different heights and weights based on position.
- Allow the user to save the data to a unique filename.
- Allow the user to input a range of birth years to generate players from.

## Known-Issues
- None


# Examples

```bash
# Generate default number(5) random players
node player-generator.js

# Generate and write default number(5) random players to the default file location
node player-generator.js -w

# Remove all data from the default file
node player-generator.js -d

# Generate 100 random players
node player-generator.js -g 100

# Generate default number(5) random Quarterbacks
node player-generator.js -p QB

# Generate default number(5) random players from Michigan
node player-generator.js -s MI

# Generate default number(5) random players from the city of Detroit
node player-generator.js -c Detroit

# Generate default number(5) random players from the city of Ann Arbor
node player-generator.js -c "Ann Arbor"

#Generate players born between 1990 and 2000
node player-generator.ks -b 1990,2000
```

## Complex Examples
```bash
# Generate 100 Kickers from Toledo Ohio and write it to the default file
node player-generator.js -g 100 -p K -c Toledo -s OH -w

#Generate 50 QBs from Michigan and write it to a file named michigan-qbs.JSON
node player-generator.js -g 50 -p QB -f michigan-qbs -w
```
