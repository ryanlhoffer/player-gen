const fs = require('fs');
const settings = require('../settings.js');

//The addPlayer function builds the player object just before sending it off to be saved to file
const addPlayer = (playerToAdd, path = settings.appSettings.defaultFileLocation) => {
    let players = fetchPlayers(path);
    players.push(playerToAdd);
    savePlayers(players, path);
};

//The fetchPlayers function fetches the players from the data file
const fetchPlayers = (path) => {
    try {
        let playersString = fs.readFileSync(path);
        return JSON.parse(playersString);
    } catch (e) {
        console.log(e);
        console.log('Something went wrong. Returning an empty array.')
        return [];
    }
};

//The savePlayers function saves players to the data file
var savePlayers = (players, path) => {
    fs.writeFileSync(path, JSON.stringify(players,null,2));
};

//The deleteFile function clears the default JSON file of data
const deleteFile = (path = settings.appSettings.defaultFileLocation) => {
    fs.unlinkSync(path);
};

module.exports = {
    addPlayer,
    deleteFile
};
