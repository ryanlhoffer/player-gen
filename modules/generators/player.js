const birthday = require('./generic/birthday');
const highschool = require('./generic/highschool');
const number = require('./generic/number');
const position = require('./generic/position');
const positionSeed = require('./generic/position-seed');
const heightWeight = require('./generic/height-weight');
const stateName = require('./generic/state-name');
const stateNameObject = require('./generic/state-name-object');
const style = require('./generic/style');
const playerStats = require('./generic/player-stats');

const Fakerator = require('fakerator');
const fakerator = Fakerator();
//The generatePlayer function assembles the player object
const generate = (argv) => {
    let player = {};

    player.firstName = fakerator.names.firstNameM();
    player.middleName = fakerator.names.firstNameM();
    player.lastName = fakerator.names.lastNameM();

    //This Ternary Operator checks to see if the command to generate a specific position is flagged.
    //If it is, pass that position to the function, otherwise run the default function
    let playerPositionSeed = argv.position ? argv.position : positionSeed.generate();
    player.position = position.generate[playerPositionSeed]();

    player.number = number.generate[player.position.shortName]();
    player.style = style.generate[player.position.shortName]()();

    let playerStateSeed = argv.state ? argv.state : stateName.generate();
    player.state = stateNameObject.generate[playerStateSeed]();

    //This Ternary Operator checks to see if the command to generate players with a specific city is flagged.
    //If it is, set the players city to the passed in value, otherwise generate one at random.
    player.city = argv.city ? argv.city : fakerator.address.city();

    player.highSchool = highschool.generate(player.city);


    player.birthday = birthday.generate(argv.birthday);
    player.stats = {};
    player.stats = heightWeight.generate[player.position.shortName]();

    const speed = playerStats.generate[player.style]();
    player.stats = { ...player.stats, speed };

    return player;
};

module.exports = { generate };
