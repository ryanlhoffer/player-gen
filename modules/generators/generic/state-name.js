const _ =require('lodash');

//If no state abbreviation is passed through, this function will return a random state
const generate = ( stateShortName = ["AL","AK","AZ","AR","CA","CO","CT","DE","FL","GA","HI","ID","IL","IN",
"IA","KS","KY","LA","ME","MD","MA","MI","MN","MS","MO","MT","NE","NV","NH","NJ","NM","NY","NC","ND","OH","OK","OR",
"PA","RI","SC","SD","TN","TX","UT","VT","VA","WA","WV","WI","WY"] ) => stateShortName[_.random(0,(stateShortName.length - 1))];

module.exports = { generate };
