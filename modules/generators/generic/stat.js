const _ = require('lodash');
const settings = require('../../../settings');

const potatoStat = () => _.random(settings.statSettings.potato.min, settings.statSettings.potato.max);
const commonStat = () => _.random(settings.statSettings.common.min,settings.statSettings.common.max);
const uncommonStat = () => _.random(settings.statSettings.uncommon.min,settings.statSettings.uncommon.max);
const rareStat = () => _.random(settings.statSettings.rare.min,settings.statSettings.rare.max);
const eliteStat = () => _.random(settings.statSettings.elite.min,settings.statSettings.elite.max);
const legendaryStat = () => _.random(settings.statSettings.legendary.min,settings.statSettings.legendary.max);

const generate = {
    potatoStat,
    commonStat,
    uncommonStat,
    rareStat,
    eliteStat,
    legendaryStat
};

module.exports = { generate };