const _ = require('lodash');

const generate = {
    "QB": function(){
        return () => {
            const playStyles = ['Spread', 'Pro', 'Hybrid'];
            return playStyles[_.random(0, (playStyles.length - 1))];
          };          
    },
    "RB": function(){
        return () =>{
           return 'None';
        };
    },
    "WR": function(){
        return () =>{
            return 'None';
         };
    },
    "TE": function(){
        return () =>{
            return 'None';
         };
    },
    "K": function(){
        return () =>{
            return 'None';
         };
    },
    "P": function(){
        return () =>{
            return 'None';
         };
    },
    "OL": function(){
        return () =>{
            return 'None';
         };
    },
    "DL": function(){
        return () =>{
            return 'None';
         };
    },
    "LB": function(){
        return () =>{
            return 'None';
         };
    },
    "DB": function(){
        return () =>{
            return 'None';
         };
    }
};

module.exports = { generate };