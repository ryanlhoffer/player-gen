const _ = require('lodash');
const stat = require('../generic/stat');

const generate = {
    "Hybrid": function(){
        const fast = _.random(0,1);
        const value = (fast) ? stat.generate.rareStat() : stat.generate.uncommonStat();
        const rarity = (fast) ? "Rare" : "Uncommon";
        const speed = {
            value,
            rarity
        };
        return speed;
    },
    "Pro": function(){
        const fast = _.random(0,1);
        const value = (fast) ? stat.generate.uncommonStat() : stat.generate.commonStat();
        const rarity = (fast) ? "Uncommon" : "Common";
        const speed = {
            value,
            rarity
        };
        return speed;
    },
    "Spread": function(){
        const fast = _.random(0,1);
        const value = (fast) ? stat.generate.eliteStat() : stat.generate.legendaryStat();
        const rarity = (fast) ? "Elite" : "Legendary";
        const speed = {
            value,
            rarity
        };
        return speed;
    },
    "None": function(){
        const value = 1;
        const rarity = "One";
        const speed = {
            value,
            rarity
        };
        return speed;
    }
};

module.exports = { generate };