const _ = require('lodash');

const generate = {
    "QB": function(){
        return _.random(1,19);
    },
    "RB": function(){
        return _.random(20,49);
    },
    "WR": function(){
        return (_.random(1,3) === 1 ) ?  _.random(10,19): _.random(80,89);
    },
    "TE": function(){
        return (_.random(1,2) === 1) ? _.random(40,49) : _.random(80,89);
    },
    "K": function(){
        return _.random(1,19);
    },
    "P": function(){
        return _.random(1,19);
    },
    "OL": function(){
        return _.random(50,79);
    },
    "DL": function(){
        return (_.random(1,4) < 4) ? _.random(50,79) : _.random(90,99);
    },
    "LB": function(){
        return (_.random(1,3) < 3) ? _.random(40,59) : _.random(90,99);
    },
    "DB": function(){
        return _.random(20,49);
    }
};

module.exports = { generate };
