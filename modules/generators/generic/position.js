//The positionNameGenerator lookup table returns an object containing the short and long name of each position
const generate = {
    "QB": function(){
        return {
            name: "Quarterback",
            shortName: "QB"
        }
    },
    "RB": function(){
        return {
            name: "Running Back",
            shortName: "RB"
        }
    },
    "WR": function(){
        return {
            name: "Wide Receiver",
            shortName: "WR"
        }
    },
    "TE": function(){
        return {
            name: "Tight End",
            shortName: "TE"
        }
    },
    "K": function(){
        return {
            name: "Kicker",
            shortName: "K"
        }
    },
    "P": function(){
        return {
            name: "Punter",
            shortName: "P"
        }
    },
    "OL": function(){
        return {
            name: "Offensive Line",
            shortName: "OL"
        }
    },
    "DL": function(){
        return {
            name: "Defensive Line",
            shortName: "DL"
        }
    },
    "LB": function(){
        return {
            name: "Linebacker",
            shortName: "LB"
        }
    },
    "DB": function(){
        return {
            name: "Defensive Back",
            shortName: "DB"
        }
    }
};

module.exports = { generate };