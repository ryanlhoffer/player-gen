const _ = require('lodash');

//The positionGenerator function randomly returns a position. An array of positions can be passed in to limit the positions generated
const generate = ( positionsShortName = ["QB","RB","WR","TE","K","P","OL","DL","LB","DB"] ) => positionsShortName[_.random(0,(positionsShortName.length - 1 ))];

module.exports = { generate };