const _ = require('lodash');

//The highSchoolGenerator randomly assigns a highschool to the players city
const generate = city => {
    let schools = ["Academy","Catholic Central","Catholic High School","Central","Christian Academy","County High School","De La Salle","East","North","Northview",
                    "Preparatory School","Senior High School","South","Southview","West"];
    (_.random(0,1)===1) ? schools = ["High School"] : schools;
    return city + " " + schools[_.random(0,(schools.length - 1))];
};

module.exports = { generate };
