const _ = require('lodash');
const settings = require('../../../settings');
//The height/weight lookup table is used to generate height/weight info
// Height: In inches to 1 point of decimal precision( 705 = 70.5 inches )
// Weight: In pounds to 1 point of decimal precision( 2950 = 295.0 pounds )
const generate = {
    "QB": function(){
        const height = _.random(settings.playerSettings.qb.height.min,settings.playerSettings.qb.height.max);
        const weight = _.random(settings.playerSettings.qb.weight.min,settings.playerSettings.qb.weight.max);
        return{ 
            height,
            weight
        };
    },
    "RB": function(){
        const height = _.random(settings.playerSettings.rb.height.min,settings.playerSettings.rb.height.max);
        const weight = _.random(settings.playerSettings.rb.weight.min,settings.playerSettings.rb.weight.max);
        return{ 
            height,
            weight
        };
    },
    "WR": function(){
        const height = _.random(settings.playerSettings.wr.height.min,settings.playerSettings.wr.height.max);
        const weight = _.random(settings.playerSettings.wr.weight.min,settings.playerSettings.wr.weight.max);
        return{ 
            height,
            weight
        };
        
    },
    "TE": function(){
        const height = _.random(settings.playerSettings.te.height.min,settings.playerSettings.te.height.max);
        const weight = _.random(settings.playerSettings.te.weight.min,settings.playerSettings.te.weight.max);

        return{ 
            height,
            weight
        };
    },
    "K": function(){
        const height = _.random(settings.playerSettings.k.height.min,settings.playerSettings.k.height.max);
        const weight = _.random(settings.playerSettings.k.weight.min,settings.playerSettings.k.weight.max);
        
        return{ 
            height,
            weight
        };
    },
    "P": function(){
        const height = _.random(settings.playerSettings.p.height.min,settings.playerSettings.p.height.max);
        const weight = _.random(settings.playerSettings.p.weight.min,settings.playerSettings.p.weight.max);
        
        return{ 
            height,
            weight
        };
    },
    "OL": function(){
        const height = _.random(settings.playerSettings.ol.height.min,settings.playerSettings.ol.height.max);
        const weight = _.random(settings.playerSettings.ol.weight.min,settings.playerSettings.ol.weight.max);
        
        return{ 
            height,
            weight
        };
    },
    "DL": function(){
        const height = _.random(settings.playerSettings.dl.height.min,settings.playerSettings.dl.height.max);
        const weight = _.random(settings.playerSettings.dl.weight.min,settings.playerSettings.dl.weight.max);

        return{ 
            height,
            weight
        };
    },
    "LB": function(){
        const height = _.random(settings.playerSettings.lb.height.min,settings.playerSettings.lb.height.max);
        const weight = _.random(settings.playerSettings.lb.weight.min,settings.playerSettings.lb.weight.max);

        return{ 
            height,
            weight
        };
    },
    "DB": function(){
        const height = _.random(settings.playerSettings.db.height.min,settings.playerSettings.db.height.max);
        const weight = _.random(settings.playerSettings.db.weight.min,settings.playerSettings.db.weight.max);
        
        return{ 
            height,
            weight
        };
    }
};

module.exports = { generate };