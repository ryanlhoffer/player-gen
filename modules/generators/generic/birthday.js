const _ = require('lodash');
const settings = require('../../../settings');
//The birthdayGenerator generates a random birthday between the two dates chosen.

const generate = (birthday) =>{
    let startingYear = settings.appSettings.birthYear.oldest;
    let endingYear = settings.appSettings.birthYear.youngest;

    if (birthday){
        const birthdayRange = birthday.split(',');
        startingYear = birthdayRange[0];
        endingYear = birthdayRange[1];
    };

    return _.random(+new Date(startingYear), +new Date(endingYear));

}
module.exports = { generate };     //These are the unix timestamps between July 1 1996 and July 1 2000
