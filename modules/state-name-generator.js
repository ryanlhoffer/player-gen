const _ =require('lodash');

//Returns the name and short name of a state when accessed via the shortname
const generator = {
    "AL": function(){
        return {
            name: "Alabama",
            shortName: "AL"
        };
    },
    "AK": function(){
        return {
            name: "Alaska",
            shortName: "AK"
        };
    },
    "AZ": function(){
        return {
            name: "Arizona",
            shortName: "AZ"
        };
    },
    "AR": function(){
        return {
            name: "Arkansas",
            shortName: "AR"
        };
    },
    "CA": function(){
        return {
            name: "California",
            shortName: "CA"
        };
    },
    "CO": function(){
        return {
            name: "Colorado",
            shortName: "CO"
        };
    },
    "CT": function(){
        return {
            name: "Connecticut",
            shortName: "CT"
        };
    },
    "DE": function(){
        return {
            name: "Delaware",
            shortName: "DE"
        };
    },
    "FL": function(){
        return {
            name: "Florida",
            shortName: "FL"
        };
    },
    "GA": function(){
        return {
            name: "Georgia",
            shortName: "GA"
        };
    },
    "HI": function(){
        return {
            name: "Hawaii",
            shortName: "HI"
        };
    },
    "ID": function(){
        return {
            name: "Idaho",
            shortName: "ID"
        };
    },
    "IL": function(){
        return {
            name: "Illinois",
            shortName: "IL"
        };
    },
    "IN": function(){
        return {
            name: "Indiana",
            shortName: "IN"
        };
    },
    "IA": function(){
        return {
            name: "Iowa",
            shortName: "IA"
        };
    },
    "KS": function(){
        return {
            name: "Kansas",
            shortName: "KS"
        };
    },
    "KY": function(){
        return {
            name: "Kentucky",
            shortName: "KY"
        };
    },
    "LA": function(){
        return {
            name: "Lousiana",
            shortName: "LA"
        };
    },
    "ME": function(){
        return {
            name: "Maine",
            shortName: "ME"
        };
    },
    "MD": function(){
        return {
            name: "Maryland",
            shortName: "MD"
        };
    },
    "MA": function(){
        return {
            name: "Massachusetts",
            shortName: "MA"
        };
    },
    "MI": function(){
        return {
            name: "Michigan",
            shortName: "MI"
        };
    },
    "MN": function(){
        return {
            name: "Minnesota",
            shortName: "MN"
        };
    },
    "MS": function(){
        return {
            name: "Mississippi",
            shortName: "MS"
        };
    },
    "MO": function(){
        return {
            name: "Missouri",
            shortName: "MO"
        };
    },
    "MT": function(){
        return {
            name: "Montana",
            shortName: "MT"
        };
    },
    "NE": function(){
        return {
            name: "Nebraska",
            shortName: "NE"
        };
    },
    "NV": function(){
        return {
            name: "Nevada",
            shortName: "NV"
        };
    },
    "NH": function(){
        return {
            name: "New Hampshire",
            shortName: "NH"
        };
    },
    "NJ": function(){
        return {
            name: "New Jersey",
            shortName: "NJ"
        };
    },
    "NM": function(){
        return {
            name: "New Mexico",
            shortName: "NM"
        };
    },
    "NY": function(){
        return {
            name: "New York",
            shortName: "NY"
        };
    },
    "NC": function(){
        return {
            name: "North Carolina",
            shortName: "NC"
        };
    },
    "ND": function(){
        return {
            name: "North Dakota",
            shortName: "ND"
        };
    },
    "OH": function(){
        return {
            name: "Ohio",
            shortName: "OH"
        };
    },
    "OK": function(){
        return {
            name: "Oklahoma",
            shortName: "OK"
        };
    },
    "OR": function(){
        return {
            name: "Oregon",
            shortName: "OR"
        };
    },
    "PA": function(){
        return {
            name: "Pennsylvania",
            shortName: "PA"
        };
    },
    "RI": function(){
        return {
            name: "Rhose Island",
            shortName: "RI"
        };
    },
    "SC": function(){
        return {
            name: "South Carolina",
            shortName: "SC"
        };
    },
    "SD": function(){
        return {
            name: "South Dakota",
            shortName: "SD"
        };
    },
    "TN": function(){
        return {
            name: "Tennessee",
            shortName: "TN"
        };
    },
    "TX": function(){
        return {
            name: "Texas",
            shortName: "TX"
        };
    },
    "UT": function(){
        return {
            name: "Utah",
            shortName: "UT"
        };
    },
    "VT": function(){
        return {
            name: "Vermont",
            shortName: "VT"
        };
    },
    "VA": function(){
        return {
            name: "Virginia",
            shortName: "VA"
        };
    },
    "WA": function(){
        return {
            name: "Washington",
            shortName: "WA"
        };
    },
    "WV": function(){
        return {
            name: "West Virginia",
            shortName: "WV"
        };
    },
    "WI": function(){
        return {
            name: "Wisconsin",
            shortName: "WI"
        };
    },
    "WY": function(){
        return {
            name: "Wyoming",
            shortName: "WY"
        };
    }
};

//If no state abbreviation is passed through, this function will return a random state
const stateGenerator = ( stateShortName = ["AL","AK","AZ","AR","CA","CO","CT","DE","FL","GA","HI","ID","IL","IN",
"IA","KS","KY","LA","ME","MD","MA","MI","MN","MS","MO","MT","NE","NV","NH","NJ","NM","NY","NC","ND","OH","OK","OR",
"PA","RI","SC","SD","TN","TX","UT","VT","VA","WA","WV","WI","WY"] ) => stateShortName[_.random(0,(stateShortName.length - 1))];

module.exports = {
    generator,
    stateGenerator
};
