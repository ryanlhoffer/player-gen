const appSettings = {
    playersToGenerate: 5,   //The Default number of players that will be generated when the app is run with no flags
    birthYear: {
        oldest: '1996',
        youngest: '2000'
    },
    defaultFileLocation: './data/player-data.JSON'
};

const statSettings = {
    potato: {
        min: 1,
        max: 19
    },
    common: {
        min: 20,
        max: 50
    },
    uncommon: {
        min:51,
        max:69
    },
    rare: {
        min:70,
        max:75
    },
    elite: {
        min: 76,
        max: 83
    },
    legendary: {
        min: 84,
        max: 89
    }
};

const qb = {
    height:{
        min: 705,
        max: 790
    },
    weight:{
        min: 1620,
        max: 2950
    }
};

const rb = {
    height:{
        min: 670,
        max: 760
    },
    weight:{
        min: 1580,
        max: 2900
    }
};

const wr = {
    height:{
        min: 670,
        max: 780
    },
    weight:{
        min: 1500,
        max: 2870
    }
};

const te = {
    height:{
        min: 720,
        max: 800
    },
    weight:{
        min: 1960,
        max: 2880
    }
};

const k = {
    height:{
        min: 670,
        max: 780
    },
    weight:{
        min: 1380,
        max: 2900
    }
};

const p = {
    height:{
        min: 680,
        max: 780
    },
    weight:{
        min: 1610,
        max: 2450
    }
};

const ol = {
    height:{
        min: 690,
        max: 830
    },
    weight:{
        min: 2100,
        max: 3950
    }
};

const dl = {
    height:{
        min: 700,
        max: 790
    },
    weight:{
        min: 1700,
        max: 3770
    }
};

const lb = {
    height:{
        min: 680,
        max: 790
    },
    weight:{
        min: 1800,
        max: 2990
    }
};

const db = {
    height:{
        min: 670,
        max: 770
    },
    weight:{
        min: 1400,
        max: 2990
    }
};

const playerSettings = {
    qb, rb, wr, te, k, p, ol, dl, lb, db
};

module.exports = {
    appSettings,
    statSettings,
    playerSettings
};
