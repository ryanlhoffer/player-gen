const settings = require('./settings.js');
const io = require('./modules/io.js');
const player = require('./modules/generators/player');
const argv = require('yargs')   //Third party package used to assist with user arguments

.options({
    h: {
        alias: 'help',
        describe: 'Show help'
    },
    b:{
        alias: 'birthday',
        describe: 'The range of years for players to be born.'
    },
    c: {
        alias: 'city',
        describe: 'Sets the city of all generated players'
    },
    d: {
        alias: 'delete',
        describe: 'Wipes the default JSON file'
    },
    f: {
        alias: 'file',
        describe: 'The name of the file to be saved'
    },
    g: {
        alias: 'generate',
        default: settings.appSettings.playersToGenerate,
        describe: 'The number of players to generate'
    },
    p:{
        alias: 'position',
        describe: 'The position to generate'
    },
    s:{
        alias: 'state',
        describe: 'Sets the state of all generated players'
    },
    v:{
        alias: 'version',
        describe: 'Show version number'
    },
    w: {
        alias: 'write',
        describe: 'Writes the generated players to a file'
    }
})
.version('0.3.0')
.argv;

//if the delete flag is set the file will be deleted
if(argv.delete) {
    let path;
    argv.file ? path = './data/' + argv.file + '.JSON' : argv.file = false;
    console.log(argv.file);
    argv.file ? io.deleteFile(path): io.deleteFile();
    return process.exit();
};

let numberOfPlayersToGenerate = argv.generate;
let playersGenerated = 0;
for (i=0;i<numberOfPlayersToGenerate;i++) {
    const newlyCreatedPlayer = player.generate(argv);
    console.log(newlyCreatedPlayer);
    if(argv.write){
        let path;
        argv.file ? path = './data/' + argv.file + '.JSON' : argv.file = false;
        argv.file ? io.addPlayer(newlyCreatedPlayer,path) : io.addPlayer(newlyCreatedPlayer);
    };
    playersGenerated ++;
};

console.log(playersGenerated," player records created.");
